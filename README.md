# Compiler

> Me trying to make my own programming language

[![Licence][licence-badge]][licence-url]
[![Release][release-badge]][release-url]

[Project home]() &bullet; [Documentation]()

.

![Compiler](screenshot.png)

## Installation

:bulb: .

:warning: !

:

```sh
git clone --recursive https://gitlab.com/carolinealing/Compiler.git
```

### Source folder



```
source/
├─
└─
```

## Usage

:

```sh

```

### Build folder



```
build/
├─
└─
```

## Versioning

Uses [Semantic Versioning](https://semver.org/). See [releases][release-url] and
[tags][tags-url] for versions of this project.

## Change-log

See [CHANGELOG.md](CHANGELOG.md) for a detailed list of changes.

## Licence

Released under MIT. See [LICENSE.md][licence-url] for more.

Coded with :heart: by [Caroline Åling][user-url].

[licence-badge]: https://badgen.net/gitlab/license/carolinealing/Compiler
[licence-url]: LICENSE
[release-badge]: https://badgen.net/gitlab/release/carolinealing/Compiler
[release-url]: https://gitlab.com/carolinealing/Compiler/-/releases
[tags-url]: https://gitlab.com/carolinealing/Compiler/-/tags
[user-url]: https://gitlab.com/carolinealing
