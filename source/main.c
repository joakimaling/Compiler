/**                    ___           __  ___
 *  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
 * / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
 * \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
 *         https://gitlab.com/carolinealing    /___/
 *
 * main.c
 * 
 * (c) 2023 Caroline Åling
 * This code is released under MIT licence. See LICENSE.md for more.
 */

int main(int argc, char **argv) {
	return 0;
}
